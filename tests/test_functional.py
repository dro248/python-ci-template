import pytest
from app import Calculator

def test_add():
    assert Calculator.add(1, 2) == 3

def test_subtract():
    assert Calculator.subtract(5, 10) == -5 

def test_multiply():
    assert Calculator.multiply(5, 10) == 50 

def test_divide():
    assert Calculator.divide(5, 10) == .5
